@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    {{Form::open(array('url'=>'/ciudad/'.$ciudad->idCiudad))}}
                        {{method_field('PUT')}}
                        <center>
                            <table>
                                <tr>
                                    <td><label for="nombre"> <b> Nombre de la ciudad: </b> </label></td>
                                    <td><input type="text" name="nombre" value="{{$ciudad->nombreCiudad}}" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="nombre"> <b> Estado al que pertenece: </b> </label></td>
                                    <td>
                                        <select name="estado" id="estados" class="form-control">
                                            @foreach($lista_estados as $item)
                                                <option value="{{ $item->id }}">{{ $item->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <a href="/ciudad" class="btn btn-danger cancelar">Cancelar</a>
                                        <input type="submit" class="btn btn-success aceptar" value="Editar">
                                    </td>
                                </tr>
                            </table>
                        </center>
                    {{Form::close()}}
                </div>
                <div class="col-4"></div>
            </div>
            <hr>
        </div>    
        <style>
            .campos{
                width: 140%;
                margin-bottom: 3%;
            }
        </style>
        <script>
            var estado = "{{ $ciudad->idEstado }}" 
            for(var indice=0; indice<document.getElementById('estados').length; indice++) {
                if (document.getElementById('estados').options[indice].value == estado )
                    document.getElementById('estados').selectedIndex = indice;
            }
        </script>
    @stop