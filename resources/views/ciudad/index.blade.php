@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                <h6>Ciudades registradas: {{ $tamano }}</h6>
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>ID estado</th>
                            <th>Nombre estado</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($lista as $item)
                            <tr>
                                <td>{{ $item->idCiudad }}</td>
                                <td>{{ $item->nombreCiudad }}</td>
                                <td>{{ $item->idEstado }}</td>
                                <td>{{ $item->nombreEstado }}</td>
                                <td></td>
                                <td>
                                    {{Form::open(array('url'=>'/ciudad/'.$item->idCiudad))}}
                                        {{method_field('DELETE')}}
                                        <input type="submit" class="btn btn-danger" value="Eliminar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/ciudad/'.$item->idCiudad.'/edit'))}}
                                        {{method_field('GET')}}
                                        <input type="submit" class="btn btn-info" value="Editar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/ciudad/'.$item->idCiudad))}}
                                        {{method_field('GET')}}
                                        <input type="submit" value="Detalle" class="btn btn-success">
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="6">
                                {!! $lista->render() !!}
                            </td>
                            <td>
                                <a href="/ciudad/create">Agregar ciudad</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    @stop