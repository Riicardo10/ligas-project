@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <center>
                        <table>
                            <tr>
                                <td> <b> ID </b></td>
                                <td> {{$ciudad->idCiudad}} </td>
                            </tr>
                            <tr>
                                <td> <b> Nombre de la ciudad: </b> </td>
                                <td> {{$ciudad->nombreCiudad}} </td>
                            </tr>
                            <tr>
                                <td> <b> Estado al que pertenece: </b> </td>
                                <td> {{$ciudad->nombreEstado}} </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><a href="/ciudad" class="btn btn-danger cancelar">Cancelar</a></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-4"></div>
            </div>
        </div>
    @stop