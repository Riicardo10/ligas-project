<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
            <script
              src="https://code.jquery.com/jquery-3.2.1.min.js"
              integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
              crossorigin="anonymous">
           </script>
            <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#btn').on('click',function(e){
                    e.preventDefault();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/data/1',
                        datatType : 'json',
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(response) {
                            alert( Object.values(response[0]));
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
            <form action="/" method="post" enctype="multipart/form-data">
                <button type="submit" id="btn"> submit</button>
            </form>
            </div>
            <div>
                <ul>
                    <li><a href="/categoria">Categoria</a></li>
                    <li><a href="/estado">Estado</a></li>
                    <li><a href="/ciudad">Ciudad</a></li>
                    <li><a href="/jugador">Jugador</a></li>
                    <li><a href="/liga">Liga</a></li>
                    <li><a href="/equipo">Equipo</a></li>
                </ul>
            </div>
        </div>
    </body>
</html>