@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                <h6>Categorías registradas: {{ $tamano }}</h6>
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($lista as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    <div style="word-break: break-all">
                                        {{ $item->nombre }}
                                    </div>
                                </td>
                                <td>
                                    <div style="word-break: break-all">
                                        {{ $item->descripcion }}
                                    </div>
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/categoria/'.$item->id))}}
                                        {{method_field('DELETE')}}
                                        <input type="submit" class="btn btn-danger" value="Eliminar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/categoria/'.$item->id.'/edit'))}}
                                        {{method_field('GET')}}
                                        <input type="submit" class="btn btn-info" value="Editar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/categoria/'.$item->id))}}
                                        {{method_field('GET')}}
                                        <input type="submit" value="Detalle" class="btn btn-success">
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4">
                                {!! $lista->render() !!}
                            </td>
                            <td>
                                <a href="/categoria/create">Agregar categoria</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    @stop