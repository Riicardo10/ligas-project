@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <center>
                        <table>
                            <tr>
                                <td> <b> ID: </b> </td>
                                <td> {{ $categoria->id }} </td>
                            </tr>
                            <tr>
                                <td> <b> Nombre de categoria: </b> </td>
                                <td> {{ $categoria->nombre }} </td>
                            </tr>
                            <tr>
                                <td> <b> Descripción: </b> </td>
                                <td> <div> {{$categoria->descripcion}} </div> </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> <a href="/categoria" class="btn btn-danger cancelar"> Cancelar </a> </td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-4"></div>
            </div>
        </div>
    @stop