@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    {{Form::open(array('url'=>'/categoria'))}}
                        <table>
                            <tr>
                                <td> <label for="nombre"> <b> Nombre de la categoria: </b> </label> </td>
                                <td> <input type="text" name="nombre" class="form-control campos" minlength="4" maxlength="40"> </td>
                            </tr>
                            <tr>
                                <td> <label for="descripcion"> <b> Descripción de la categoria: </b> </label> </td>
                                <td> <textarea name="descripcion" cols="30" rows="3" class="form-control"></textarea> </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a href="/categoria" class="btn btn-danger cancelar">Cancelar</a>
                                    <input type="submit" class="btn btn-success aceptar" value="Agregar">
                                </td>
                            </tr>
                        </table>
                    {{Form::close()}}
                </div>
                <div class="col-3"></div>
            </div>
        </div>
        <style>
            .campos{
                width: 140%;
                margin-bottom: 3%;
            }
        </style>
    @stop
    