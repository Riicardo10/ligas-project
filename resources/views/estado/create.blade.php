@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>  
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    {{Form::open(array('url'=>'/estado'))}}
                        <table>
                            <tr>
                                <td> <label for="nombre"> <b> Nombre del estado: </b> </label> </td>
                                <td> <input type="text"name="nombre" class="form-control campos"> </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a href="/estado" class="btn btn-danger cancelar">Cancelar</a>
                                    <input type="submit" class="btn btn-success aceptar" value="Agregar">
                                </td>
                            </tr>
                        </table>
                    {{Form::close()}}
                </div>
                <div class="col-4"></div>
            </div>
        </div>
        <style>
            .campos{
                width: 140%;
                margin-bottom: 3%;
            }
        </style>
    @stop