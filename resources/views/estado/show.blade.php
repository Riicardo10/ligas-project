@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <center>
                        <table>
                            <tr>
                                <td> <b> ID: </b> </td>
                                <td> {{$estado->id}} </td>
                            </tr>
                            <tr>
                                <td> <b> Nombre del estado: </b> </td>
                                <td> {{$estado->nombre}} </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> <a href="/estado" class="btn btn-danger cancelar"> Cancelar </a> </td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-4"></div>
            </div>
        </div>
    @stop