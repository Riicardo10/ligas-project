@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                <h6>Estados registrados: {{ $tamano }}</h6>
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($lista as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->nombre }}</td>
                                <td>
                                    {{Form::open(array('url'=>'/estado/'.$item->id))}}
                                        {{method_field('DELETE')}}
                                        <input type="submit" class="btn btn-danger" value="Eliminar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/estado/'.$item->id.'/edit'))}}
                                        {{method_field('GET')}}
                                        <input type="submit" class="btn btn-info" value="Editar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/estado/'.$item->id))}}
                                        {{method_field('GET')}}
                                        <input type="submit" value="Detalle" class="btn btn-success">
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3">
                                {!! $lista->render() !!}
                            </td>
                            <td><a href="/estado/create">Agregar estado</a></td>
                        </tr>
                    </table>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    @stop