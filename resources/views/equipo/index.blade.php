@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <h6>Equipos registrados: {{ $tamano }}</h6>
        <div class="content">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                    <table class="table">
                        <tr>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>Rama</th>
                            <th>Liga</th>
                            <th>Año</th>
                            <th>Categoria</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($lista as $item)
                            <tr>
                                <td>
                                    <div style="word-break: break-all">
                                        {{ $item->claveEquipo }}
                                    </div>
                                <td>
                                    <div style="word-break: break-all">
                                        {{ $item->nombreEquipo }}
                                    </div>
                                </td>
                                <td> {{ $item->ramaEquipo }} </td>
                                <td> {{ $item->nombreLiga }} </td>
                                <td> {{ $item->anioLiga }} </td>
                                <td> {{ $item->nombreCategoria }} </td>
                                <td>
                                    {{Form::open(array('url'=>'/equipo/'.$item->claveEquipo))}}
                                        {{method_field('DELETE')}}
                                        <input type="submit" class="btn btn-danger" value="Eliminar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/equipo/'.$item->claveEquipo.'/edit'))}}
                                        {{method_field('GET')}}
                                        <input type="submit" class="btn btn-info" value="Editar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/equipo/'.$item->claveEquipo))}}
                                        {{method_field('GET')}}
                                        <input type="submit" value="Detalle" class="btn btn-success">
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="7">
                                {!! $lista->render() !!}
                            </td>
                            <td>
                                <a href="/equipo/create">Agregar equipo</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    @stop