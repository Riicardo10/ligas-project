@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    {{Form::open(array('url'=>'/equipo/'.$equipo->claveEquipo, 'onsubmit'=>'return verificar()'))}}
                        {{method_field('PUT')}}
                        <table>
                            <tr>
                                <td><label for="nombre"> <b> Nombre de equipo: </b> </label></td>
                                <td><input type="text" name="nombre" class="form-control campos" value="{{$equipo->nombreEquipo}}" required></td>
                            </tr>
                            <tr>
                                <td><label for="rama"> <b> Rama: </b> </label></td>
                                <td>
                                    <select name="rama" id="rama" class="form-control" style="width: 100%">
                                        <option value="V">Varonil</option>
                                        <option value="F">Femenino</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="contacto"> <b> Contacto: </b> </label></td>
                                <td><input type="text" name="contacto" class="form-control campos" value="{{$equipo->contactoEquipo}}" required></td>
                            </tr>
                            <tr>
                                <td><label for="email"> <b> Email: </b> </label></td>
                                <td><input type="text" name="email" class="form-control campos" value="{{$equipo->emailEquipo}}" required></td>
                            </tr>
                            <tr>
                                <td><label for="usuario">Usuario:</label></td>
                                <td><input id="usuario" type="text" name="usuario" class="form-control campos" minlength="4" value="{{$equipo->usuarioEquipo}}" required readonly></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><label id="usuario_existe" style="display: none;" style="margin: 1%;">El nombre de usuario ya está ocupado.</label></td>
                            </tr>
                            <tr>
                                <td><label for="estado"> <b> Estado: </b> </label></td>
                                <td>
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="0" >Selecciona estado:</option>
                                        @foreach($lista_estados as $item)
                                            <option value="{{ $item->id }}" >{{ $item->nombre }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="ciudad"> <b> Ciudad: </b> </label></td>
                                <td>
                                    <select name="ciudad" id="ciudad" class="form-control"  size="3">
                                        <option value="0" >Selecciona ciudad</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="liga"> <b> Liga: </b> </label></td>
                                <td>
                                    <select name="liga" id="liga" class="form-control" size="3">
                                        <option value="0" >Selecciona liga</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="categoria"> <b> Categoria: </b> </label></td>
                                <td>
                                    <select name="categoria" id="categoria" class="form-control" size="3" required>
                                        @foreach($lista_categorias as $item)
                                            <option value="{{ $item->id }}">{{ $item->nombre }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a href="/equipo" class="btn btn-danger cancelar">Cancelar</a>
                                    <input type="submit" class="btn btn-success aceptar" value="Agregar">
                                </td>
                            </tr>
                        </table>
                    {{Form::close()}}
                </div>
                <div class="col-4"></div>
            </div>
        </div>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script type="text/javascript">
            var ya_usuario = false;
            $(document).ready(function(){
                $("#estado").change(function( e ){
                    e.preventDefault();
                    var idEstado = $("#estado").val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/ciudad/ciudades/json/'+idEstado,
                        datatType : 'json',
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(response) {
                            $("#ciudad").find('option').remove();
                            $("#liga").find('option').remove();
                            for(var i=0; i<response.length; i++){
                                $("#ciudad").append('<option value="' + Object.values(response[i])[0] + '">' + Object.values(response[i])[1] + '</option>');
                            }
                        }
                    });
                    $("#ciudad").prop('disabled', false);
                });
                $("#ciudad").change(function( e ){
                    e.preventDefault();
                    var idCiudad = $("#ciudad").val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/liga/ciudad/json/'+idCiudad,
                        datatType : 'json',
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(response) {
                            $("#liga").find('option').remove();
                            for(var i=0; i<response.length; i++){
                                $("#liga").append('<option value="' + Object.values(response[i])[0] + '">' + Object.values(response[i])[1] + " || " + Object.values(response[i])[2]  + '</option>');
                            }
                        }
                    });
                    $("#liga").prop('disabled', false);
                });
            });
        </script>
        <script>
            function verificar(){
                var estado = document.getElementById("estado");
                var ciudad = document.getElementById("ciudad");
                var liga = document.getElementById("liga");
                if( estado.value == 0 ) {
                    alert( 'Selecciona estado de la liga' );
                    return false;
                }
                if( ciudad.value == 0 ) {
                    alert( 'Selecciona ciudad de la liga' );
                    return false;
                }
                if( liga.value == 0 ) {
                    alert( 'Selecciona liga' );
                    return false;
                }
                return true;
            }
        </script>
        <script>
            var idEstado = "{{ $equipo->idEstado }}" 
            for(var indice=0; indice<document.getElementById('estado').length; indice++) {
                if (document.getElementById('estado').options[indice].value == idEstado )
                    document.getElementById('estado').selectedIndex = indice;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/ciudad/ciudades/json/'+idEstado,
                datatType : 'json',
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                success:function(response) {
                    $("#ciudad").find('option').remove();
                    for(var i=0; i<response.length; i++){
                        $("#ciudad").append('<option value="' + Object.values(response[i])[0] + '">' + Object.values(response[i])[1] + '</option>');
                    }
                    var idCiudad = "{{ $equipo->idCiudad }}" 
                    for(var indice=0; indice<document.getElementById('ciudad').length; indice++) {
                        if (document.getElementById('ciudad').options[indice].value == idCiudad ){
                            document.getElementById('ciudad').selectedIndex = indice;
                            break;
                        }
                    }
                }
            });    
        </script>
        <script>
            //CARGA DE DATOS
            var idCiudad = "{{ $equipo->idCiudad }}"
            var idLiga = "{{ $equipo->idLiga }}"
            var idCategoria = "{{ $equipo->idCategoria }}"
            var rama = "{{ $equipo->ramaEquipo }}"
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/liga/ciudad/json/'+idCiudad,
                datatType : 'json',
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                success:function(response) {
                    $("#liga").find('option').remove();
                    for(var i=0; i<response.length; i++){
                        $("#liga").append('<option value="' + Object.values(response[i])[0] + '">' + Object.values(response[i])[1] + " || " + Object.values(response[i])[2]  + '</option>');
                    }
                    for(var indice=0; indice<document.getElementById('liga').length; indice++) {
                        if (document.getElementById('liga').options[indice].value == idLiga )
                            document.getElementById('liga').selectedIndex = indice;
                    }
                }
            });

            for(var indice=0; indice<document.getElementById('rama').length; indice++) {
                if (document.getElementById('rama').options[indice].value == rama )
                    document.getElementById('rama').selectedIndex = indice;
            }

            for(var indice=0; indice<document.getElementById('categoria').length; indice++) {
                if (document.getElementById('categoria').options[indice].value == idCategoria )
                    document.getElementById('categoria').selectedIndex = indice;
            }

        </script>
    @stop