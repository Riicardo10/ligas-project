@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <center>
                        <table>
                            <tr>
                                <td> <b> Clave: </b> </td>
                                <td> {{ $equipo->claveEquipo }} </td>
                            </tr>
                            <tr>
                                <td> <b> Nombre del equipo: </b> </td>
                                <td> {{ $equipo->nombreEquipo }} </td>
                            </tr>
                            <tr>
                                <td> <b> Rama: </b> </td>
                                <td> {{ $equipo->ramaEquipo }} </td>
                            </tr>
                            <tr>
                                <td> <b> Contacto: </b> </td>
                                <td> {{ $equipo->contactoEquipo }} </td>
                            </tr>
                            <tr>
                                <td> <b> Email del responsable: </b> </td>
                                <td> {{ $equipo->emailEquipo }} </td>
                            </tr>
                            <tr>
                                <td> <b> Usuario: </b> </td>
                                <td> {{ $equipo->usuarioEquipo }} </td>
                            </tr>
                            <tr>
                                <td> <b> Liga: </b> </td>
                                <td> {{ $equipo->nombreLiga }} </td>
                            </tr>
                            <tr>
                                <td> <b> Ciudad de la liga: </b> </td>
                                <td> {{ $equipo->nombreCiudad }}, {{ $equipo->nombreEstado }} </td>
                            </tr>
                            <tr>
                                <td> <b> Categoria: </b> </td>
                                <td> {{ $equipo->nombreCategoria }} </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><a href="/equipo" class="btn btn-danger cancelar">Cancelar</a></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-4"></div>
            </div>
        </div>
    @stop