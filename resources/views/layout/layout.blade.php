<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield( 'titulo' ) </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            html, body {
                color: #777;
                font-family: 'Raleway', sans-serif;
                margin-top: 1%;
            }
            .content {
                text-align: center;
            }
            h1, h6{
                text-align: center;
            }
            .cancelar{
                margin-top: 3%;
            }
            .aceptar{
                margin-top: 3%;
            }
            .label-detalle{
                padding-right: 0%;
            }
        </style>
    </head>
    <body>
        @yield('content')
    </body>
    <script>
        
    </script>
</html>