@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                <h6>Jugadores registrados: {{ $tamano }}</h6>
                    <table class="table">
                        <tr>
                            <th>CURP</th>
                            <th>Nombre completo</th>
                            <th>Foto</th>
                            <th>Fecha de nacimiento</th>
                            <th>Sexo</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($lista as $item)
                            <tr>
                                <td>{{ $item->curp }}</td>
                                <td>{{ $item->nombreCompleto }}</td>
                                @if( $item->foto )
                                    <td>
                                        <img src="/storage/{{$item->foto}}" alt="Error" width="100px;" class="rounded-circle">
                                    </td>
                                @else
                                    <td>Sin foto</td>
                                @endif
                                <td>{{ $item->fechaNacimiento }}</td>
                                @if( $item->sexo == 'M' )
                                    <td>Masculino</td>
                                @else
                                    <td>Femenino</td>
                                @endif
                                <td></td>
                                <td>
                                    {{Form::open(array('url'=>'/jugador/'.$item->curp))}}
                                        {{method_field('DELETE')}}
                                        <input type="submit" class="btn btn-danger" value="Eliminar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/jugador/'.$item->curp.'/edit'))}}
                                        {{method_field('GET')}}
                                        <input type="submit" class="btn btn-info" value="Editar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/jugador/'.$item->curp))}}
                                        {{method_field('GET')}}
                                        <input type="submit" value="Detalle" class="btn btn-success">
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="7">
                                {!! $lista->render() !!}
                            </td>
                            <td><a href="/jugador/create">Agregar jugador</a></td>
                        </tr>
                    </table>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    @stop