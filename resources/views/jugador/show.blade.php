@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    <center>
                        <table>
                            <tr>
                                <td> <b> CURP: </b> </td>
                                <td> {{$jugador->curp}} </td>
                            </tr>
                            <tr>
                                <td> <b> Nombre completo: </b> </td>
                                <td> {{$jugador->nombreCompleto}} </td>
                            </tr>
                            @if( !$jugador->foto )
                                <tr>
                                    <td> <b> Foto: </b> </td>
                                    <td> Sin foto </td>
                                </tr>
                            @else
                                <tr>
                                    <td> <b> Foto: </b> </td>
                                    <td>
                                        <img src="/storage/{{$jugador->foto}}" alt="Error" width="250px;" style="margin: 5%;" class="img-thumbnail">
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td> <b> Fecha de nacimiento: </b> </td>
                                <td> {{$jugador->fechaNacimiento}} </td>
                            </tr>
                            <tr>
                                <td> <b> Sexo: </b> </td>
                                @if($jugador->sexo == 'M')
                                    <td> Masculino </td>
                                @elseif($jugador->sexo == 'F')
                                <td> Femenino </td>
                                @endif
                            </tr>
                            <tr>
                                <td></td>
                                <td><a href="/jugador" class="btn btn-danger cancelar">Cancelar</a></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-3"></div>
            </div>
        </div>
    @stop