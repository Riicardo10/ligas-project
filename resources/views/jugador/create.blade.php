@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    {{Form::open(array('url'=>'/jugador', 'enctype'=>'multipart/form-data'))}}
                        <center>
                            <table>
                                <tr>
                                    <td><label for="curp"> <b> CURP: </b> </label></td>
                                    <td><input type="text" name="curp" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="nombre"> <b> Nombre completo: </b> </label></td>
                                    <td><input type="text" name="nombre" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="foto"> <b> Foto: </b> </label></td>
                                    <td><input type="file" name="foto" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="fecha_nacimiento"> <b> Fecha de nacimiento: </b> </label></td>
                                    <td><input type="date" name="fecha_nacimiento" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="sexo"> <b> Sexo: </b> </label></td>
                                    <td>
                                        <select name="sexo" id="" class="form-control" style="width: 100%">
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <a href="/jugador" class="btn btn-danger cancelar">Cancelar</a>
                                        <input type="submit" class="btn btn-success aceptar" value="Agregar">
                                    </td>
                                </tr>
                            </table>
                        </center>
                    {{Form::close()}}
                </div>
                <div class="col-4"></div>
            </div>
        </div>
        <style>
            .campos{
                width: 140%;
                margin-bottom: 3%;
            }
        </style>
    @stop