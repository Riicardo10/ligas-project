@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    {{Form::open(array('url'=>'/jugador/'.$jugador->curp, 'enctype'=>'multipart/form-data'))}}
                        {{method_field('PUT')}}
                        <center>
                            <table>
                                <tr>
                                    <td><label for="nombre"> <b> Nombre completo: </b> </label></td>
                                    <td><input type="text" name="nombre" value="{{$jugador->nombreCompleto}}" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for=""> <b> Foto: </b> </label></td>
                                    <td>
                                        <img src="/storage/{{$jugador->foto}}" alt="Error" width="250px;" style="margin: 5%;" class="img-thumbnail">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="foto"> <b> Cambiar foto: </b> </label></td>
                                    <td><input type="file" name="foto" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="fecha_nacimiento"> <b> Fecha de nacimiento: </b> </label></td>
                                    <td><input type="date" name="fecha_nacimiento" value="{{$jugador->fechaNacimiento}}" class="form-control campos"></td>
                                </tr>
                                <tr>
                                    <td><label for="sexo"> <b> Sexo: </b> </label></td>
                                    <td>
                                        <select name="sexo" id="sexo" class="form-control" style="width:60%">
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <a href="/jugador" class="btn btn-danger cancelar">Cancelar</a>
                                        <input type="submit" class="btn btn-success aceptar" value="Editar">
                                    </td>
                                </tr>
                            </table>
                        </center>
                    {{Form::close()}}
                </div>
                <div class="col-4"></div>
            </div>
        </div>
        <script>
            var sexo = "{{ $jugador->sexo }}" 
            for(var indice=0; indice<document.getElementById('sexo').length; indice++) {
                if (document.getElementById('sexo').options[indice].value == sexo )
                    document.getElementById('sexo').selectedIndex = indice;
            }
        </script>
        <style>
            .campos{
                width: 120%;
                margin-bottom: 3%;
            }
        </style>
    @stop