@extends('layout.layout')
    @section('titulo', $titulo)
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    {{Form::open(array('url'=>'/liga/'.$liga->idLiga, 'onsubmit'=>'return verificar()'))}}
                        {{method_field('PUT')}}
                        <table>
                            <tr>
                                <td> <label for="nombre"> <b> Nombre de la liga: </b> </label></td>
                                <td><input type="text" placeholder="" name="nombre" class="form-control campos" value="{{$liga->nombreLiga}}" required></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="hidden" placeholder="" name="anio" class="form-control campos" value="{{$liga->anioLiga}}" readonly></td>
                            </tr>
                            <tr>
                                <td> <label for="email"> <b> Email: </b> </label></td>
                                <td><input type="text" placeholder="" name="email" class="form-control campos" value="{{$liga->emailLiga}}" required></td>
                            </tr>
                            <tr>
                                    <td> <label for="usuario"> <b> Usuario: </b> </label></td>
                                <td><input id="usuario" type="text" placeholder="" name="usuario" class="form-control campos" value="{{$liga->usuarioLiga}}" readonly></td>
                            </tr>
                            <tr>
                                    <td> <label for="estado"> <b> Estado: </b> </label></td>
                                <td>
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="0" >Selecciona estado</option>
                                        @foreach($lista_estados as $item)
                                            <option value="{{ $item->id }}" >{{ $item->nombre }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                    <td> <label for="ciudad"> <b> Ciudad: </b> </label></td>
                                <td>
                                    <select name="ciudad" id="ciudad" class="form-control">
                                        <option value="0" >Selecciona ciudad</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a href="/liga" class="btn btn-danger" style="margin-top: 3%;">Cancelar</a>
                                    <input type="submit" class="btn btn-success" value="Agregar" style="margin-top: 3%;">
                                </td>
                            </tr>
                        </table>
                    {{Form::close()}}
                </div>
                <div class="col-4"></div>
            </div>
        </div>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"> </script>
        <script type="text/javascript">
                $(document).ready(function(){
                $("#estado").change(function( e ){
                    e.preventDefault();
                    var idEstado = $("#estado").val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/ciudad/ciudades/json/'+idEstado,
                        datatType : 'json',
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(response) {
                            $("#ciudad").find('option').remove();
                            $("#ciudad").append('<option value="0">Selecciona ciudad</option>');
                            for(var i=0; i<response.length; i++){
                                $("#ciudad").append('<option value="' + Object.values(response[i])[0] + '">' + Object.values(response[i])[1] + '</option>');
                            }
                        }
                    });
                    $("#ciudad").prop('disabled', false);
                });
            });
        </script>
        <script>
            function verificar(){
                var ciudad = document.getElementById("ciudad");
                if( ciudad.value == 0 ) {
                    alert( 'Selecciona estado/ciudad de la liga' );
                    return false;
                }
                return true;
            }
        </script>
        <script>
            var idEstado = "{{ $liga->idEstado }}" 
            for(var indice=0; indice<document.getElementById('estado').length; indice++) {
                if (document.getElementById('estado').options[indice].value == idEstado )
                    document.getElementById('estado').selectedIndex = indice;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/ciudad/ciudades/json/'+idEstado,
                datatType : 'json',
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                success:function(response) {
                    $("#ciudad").find('option').remove();
                    $("#ciudad").append('<option value="0">Selecciona ciudad</option>');
                    for(var i=0; i<response.length; i++){
                        $("#ciudad").append('<option value="' + Object.values(response[i])[0] + '">' + Object.values(response[i])[1] + '</option>');
                    }
                    var idCiudad = "{{ $liga->idCiudad }}" 
                    for(var indice=0; indice<document.getElementById('ciudad').length; indice++) {
                        if (document.getElementById('ciudad').options[indice].value == idCiudad ){
                            document.getElementById('ciudad').selectedIndex = indice;
                            break;
                        }
                    }
                }
            });    
        </script>
    @stop