@extends('layout.layout')
    @section('titulo', $titulo)
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    <center>
                        <table>
                            <tr>
                                <td> <b> ID: </b> </td>
                                <td> {{ $liga->idLiga }} </td>
                            </tr>
                            <tr>
                                <td> <b> Nombre de la liga: </b> </td>
                                <td> {{ $liga->nombreLiga }} </td>
                            </tr>
                            <tr>
                                <td> <b> Año: </b> </td>
                                <td> {{ $liga->anioLiga }} </td>
                            </tr>
                            <tr>
                                <td> <b> Email del responsable: </b> </td>
                                <td> {{ $liga->emailLiga }} </td>
                            </tr>
                            <tr>
                                <td> <b> Usuario: </b> </td>
                                <td> {{ $liga->usuarioLiga }} </td>
                            </tr>
                            <tr>
                                <td> <b> Ciudad: </b> </td>
                                <td> {{ $liga->nombreCiudad }} </td>
                            </tr>
                            <tr>
                                <td> <b> Estado: </b> </td>
                                <td> {{ $liga->nombreEstado }} </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><a href="/liga" class="btn btn-danger cancelar">Cancelar</a></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <div class="col-3"></div>
            </div>
        </div>
    @stop