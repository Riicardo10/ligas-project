@extends('layout.layout')
    @section('titulo', $titulo )
    @section('content')
        <h1> {{$titulo}} </h1>
        <div class="content">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                <h6>Ligas registradas: {{ $tamano }}</h6>
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Año</th>
                            <th>Email</th>
                            <th>Usuario</th>
                            <th>Ciudad</th>
                            <th>Estado</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($lista as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    <div style="word-break: break-all">
                                        {{ $item->nombre }}
                                    </div>
                                </td>
                                <td> {{ $item->anio }} </td>
                                <td> {{ $item->email }} </td>
                                <td> {{ $item->usuario }} </td>
                                <td> {{ $item->nombreCiudad }} </td>
                                <td> {{ $item->nombreEstado }} </td>
                                <td>
                                    {{Form::open(array('url'=>'/liga/'.$item->id))}}
                                        {{method_field('DELETE')}}
                                        <input type="submit" class="btn btn-danger" value="Eliminar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/liga/'.$item->id.'/edit'))}}
                                        {{method_field('GET')}}
                                        <input type="submit" class="btn btn-info" value="Editar">
                                    {{Form::close()}}
                                </td>
                                <td>
                                    {{Form::open(array('url'=>'/liga/'.$item->id))}}
                                        {{method_field('GET')}}
                                        <input type="submit" value="Detalle" class="btn btn-success">
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="8">
                                {!! $lista->render() !!}
                            </td>
                            <td>
                                <a href="/liga/create">Agregar liga</a>
                            </td>
                        </tr>
                    </table>
                    <br>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    @stop