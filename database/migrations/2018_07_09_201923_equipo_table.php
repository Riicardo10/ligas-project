<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EquipoTable extends Migration {
    
    public function up() {
        Schema::create('equipo', function (Blueprint $table) {
            $table->string('clave', 40);
            $table->primary('clave');
            $table->string('nombre', 100);
            $table->string('rama', 1)->nullable();
            $table->string('contacto', 100)->nullable();
            $table->string('email', 360);
            $table->string('usuario', 100);
            $table->string('contrasenia', 15);
            $table->integer('idLiga')->unsigned();
            $table->foreign('idLiga')->references('id')->on('liga');
            $table->integer('idCategoria')->unsigned();
            $table->foreign('idCategoria')->references('id')->on('categoria');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('equipo');
    }
}
