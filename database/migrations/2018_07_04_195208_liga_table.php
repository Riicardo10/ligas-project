<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LigaTable extends Migration {
    
    public function up() {
        Schema::create('liga', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100)->nullable();
            $table->integer('anio');
            $table->string('email', 360);
            $table->string('usuario', 100)->unique();
            $table->string('contrasenia', 15);
            $table->integer('idCiudad')->unsigned();
            $table->foreign('idCiudad')->references('id')->on('ciudad');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('liga');
    }
}
