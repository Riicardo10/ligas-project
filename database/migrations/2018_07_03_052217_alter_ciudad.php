<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCiudad extends Migration {
    
    public function up() {
        Schema::table('ciudad', function (Blueprint $table) {
            // $table->integer('idEstado')->references('id')->on('estado');
            // $table->integer('idEstado', 10);
            // $table->foreign('idEstado')->references('id')->on('estado');
        });
    }

    public function down() {
        Schema::table('ciudad', function (Blueprint $table) {
            Schema::dropIfExists('ciudad');            
        });
    }
}
