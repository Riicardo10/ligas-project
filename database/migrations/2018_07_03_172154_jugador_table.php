<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JugadorTable extends Migration {
    
    public function up() {
        Schema::create('jugador', function (Blueprint $table) {
            $table->string('curp', 18);
            $table->primary('curp');
            $table->string('nombreCompleto', 150);
            $table->string('foto', 360)->nullable();
            $table->date('fechaNacimiento')->nullable();
            $table->string('sexo', 1)->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('jugador');
    }
}
