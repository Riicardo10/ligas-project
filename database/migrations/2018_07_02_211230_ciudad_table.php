<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CiudadTable extends Migration {
    public function up() {
        Schema::create('estado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->timestamps();
        });
        Schema::create('ciudad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('idEstado')->unsigned();
            $table->foreign('idEstado')->references('id')->on('estado');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('ciudad');
    }
}
