<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\EstadoModel;
use App\EquipoModel;
use App\CategoriaModel;

class EquipoControl extends Controller {
    public function index() {
        $lista = DB::table('equipo')
            ->join( 'liga', 'equipo.idLiga', '=', 'liga.id' )
            ->join( 'categoria', 'equipo.idCategoria', '=', 'categoria.id' )
            ->select( 'equipo.clave AS claveEquipo', 'equipo.nombre AS nombreEquipo', 'equipo.rama AS ramaEquipo', 'liga.nombre AS nombreLiga', 'liga.anio AS anioLiga', 'categoria.nombre AS nombreCategoria' )
            ->orderBy( 'equipo.nombre', 'ASC' )
            ->paginate( 5 );
        $tamano = count( EquipoModel::all() );
        $titulo = 'Lista de equipos registrados.';
        return view( '/equipo/index', compact( 'lista', 'tamano', 'titulo' ) );
    }

    public function create(){
        $lista_estados = EstadoModel::all();
        $lista_categorias = CategoriaModel::all();
        $titulo = 'Registro de equipos.';
        return view( '/equipo/create', compact( 'lista_estados', 'lista_categorias', 'titulo' ));
    }

    public function store(Request $req) {
        $anio = DB::table('liga')->select('liga.anio')->where( 'id', '=', $req->liga )->get();    
        $split = explode(':', $anio);
        $anio = (int) $split[1];
        $equipo = new EquipoModel;
        $equipo->clave = strtolower( $req->usuario ) . $req->rama . $anio;
        $equipo->nombre = ucfirst( $req->nombre );
        $equipo->rama = strtoupper( $req->rama );
        $equipo->contacto = ucwords( $req->contacto );
        $equipo->email = $req->email;
        $equipo->usuario = $req->usuario;
        $equipo->contrasenia = $req->contrasenia;
        $equipo->idLiga = $req->liga;
        $equipo->idCategoria = $req->categoria;
        $equipo->save();
        return redirect( '/equipo' );
    }

    public function show($clave) {
        $equipo = DB::table('equipo')
            ->join( 'liga', 'equipo.idLiga', '=', 'liga.id' )
            ->join( 'categoria', 'equipo.idCategoria', '=', 'categoria.id' )
            ->join( 'ciudad', 'liga.idCiudad', '=', 'ciudad.id' )
            ->join( 'estado', 'ciudad.idEstado', '=', 'estado.id' )
            ->select( 'equipo.clave AS claveEquipo', 'equipo.nombre AS nombreEquipo', 
                        'equipo.rama AS ramaEquipo', 'equipo.contacto AS contactoEquipo',
                        'equipo.email AS emailEquipo', 'equipo.usuario AS usuarioEquipo',
                        'liga.nombre AS nombreLiga', 'liga.anio AS anioLiga', 'categoria.nombre AS nombreCategoria',
                        'ciudad.nombre AS nombreCiudad', 'estado.nombre AS nombreEstado' )
            ->where( 'clave', '=', $clave )->get()->first();
        $titulo = 'Detalle de equipo.';
        return view( '/equipo/show', compact( 'equipo', 'titulo' ) );
    }

    public function edit($clave) {
        $equipo = DB::table('equipo')
            ->join( 'liga', 'equipo.idLiga', '=', 'liga.id' )
            ->join( 'categoria', 'equipo.idCategoria', '=', 'categoria.id' )
            ->join( 'ciudad', 'liga.idCiudad', '=', 'ciudad.id' )
            ->join( 'estado', 'ciudad.idEstado', '=', 'estado.id' )
            ->select( 'equipo.clave AS claveEquipo', 'equipo.nombre AS nombreEquipo', 
                        'equipo.rama AS ramaEquipo', 'equipo.contacto AS contactoEquipo',
                        'equipo.email AS emailEquipo', 'equipo.usuario AS usuarioEquipo', 'liga.id AS idLiga',
                        'liga.nombre AS nombreLiga', 'liga.anio AS anioLiga', 'categoria.nombre AS nombreCategoria', 'categoria.id AS idCategoria', 
                        'ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'estado.id AS idEstado', 'estado.nombre AS nombreEstado' )
            ->where( 'clave', '=', $clave )->get()->first();
        $lista_estados = EstadoModel::all();
        $lista_categorias = CategoriaModel::all();
        $titulo = 'Edición de equipo.';
        return view( '/equipo/edit', compact( 'lista_estados', 'lista_categorias', 'equipo', 'titulo' ));
    }

    public function update(Request $req, $clave) {
        EquipoModel::where( 'clave', $clave )->update( [
            'nombre'=>ucfirst($req->nombre),
            'rama'=>strtoupper($req->rama),
            'contacto'=>ucwords( $req->contacto ),
            'email'=>$req->email,
            'idLiga'=>$req->liga,
            'idCategoria'=>$req->categoria
        ] );
        return redirect( '/equipo' );
    }

    public function destroy($clave) {
        $equipo = EquipoModel::where('clave', '=', $clave)->delete();
        return redirect( '/equipo' );
    }
}
