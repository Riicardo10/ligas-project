<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\CiudadModel;
use App\EstadoModel;

class CiudadControl extends Controller {
    public function index () {
        $lista = DB::table('ciudad')
            ->join('estado', 'ciudad.idEstado', '=', 'estado.id')
            ->select('ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'ciudad.idEstado as idEstado', 'estado.nombre AS nombreEstado')
            ->orderBy('ciudad.id', 'ASC')
            ->paginate( 2 );
        $tamano = count( CiudadModel::all() );
        $titulo = 'Lista de ciudades registradas.';
        return view( '/ciudad/index', compact( 'lista', 'tamano', 'titulo' ) );
    }

    public function create () {
        $lista_estados = EstadoModel::all();
        $titulo = 'Registro de ciudades.';
        return view( '/ciudad/create', compact( 'lista_estados', 'titulo' ) );
    }

    public function store ( Request $req ) {
        $ciudad = new CiudadModel;
        $ciudad->nombre = ucwords( $req->nombre );
        $ciudad->idEstado = $req->estado;
        $ciudad->save();
        return redirect( '/ciudad' );
    }

    public function show ( $id ) {
        $ciudad = DB::table('ciudad')
            ->join('estado', 'ciudad.idEstado', '=', 'estado.id')
            ->select('ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'ciudad.idEstado as idEstado', 'estado.nombre AS nombreEstado')
            ->where('ciudad.id', '=', $id)
            ->get()->first();
        $titulo = 'Detalle de ciudad.';
        return view( '/ciudad/show', compact( 'ciudad', 'titulo' ) );
    }

    public function edit ( $id ) {
        $lista_estados = EstadoModel::all();
        $ciudad = DB::table('ciudad')
            ->join('estado', 'ciudad.idEstado', '=', 'estado.id')
            ->select('ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'ciudad.idEstado as idEstado', 'estado.nombre AS nombreEstado')
            ->where('ciudad.id', '=', $id)
            ->get()->first();
        $titulo = 'Edición de ciudad.';
        return view( '/ciudad/edit', compact( 'ciudad', 'lista_estados', 'titulo' ) );
    }

    public function update ( Request $req, $id ) {
        CiudadModel::where( 'id', $id )->update( [
            'nombre'=>$req->nombre,
            'idEstado'=>$req->estado
        ] );
        return redirect( '/ciudad' );
    }

    public function destroy ( $id ) {
        $ciudad = CiudadModel::find( $id );
        $ciudad->delete( );
        return redirect( '/ciudad' );
    }
}
