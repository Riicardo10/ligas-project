<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\JugadorModel;
use DB;

class JugadorControl extends Controller {
    public function index() {
        $lista = JugadorModel::orderBy( 'curp', 'ASC' )->paginate( 5 );
        $tamano = count( JugadorModel::all() );
        $titulo = 'Lista de jugadores registrados.';
        return view( '/jugador/index', compact( 'lista', 'tamano', 'titulo' ) );
    }

    public function create() {
        $titulo = 'Registro de jugadores.';
        return view( '/jugador/create', compact( 'titulo' ) );
    }

    public function store(Request $req) {
        $jugador = new JugadorModel;
        $jugador->curp = strtoupper($req->curp);
        $jugador->nombreCompleto = ucwords($req->nombre);

        if( $req->hasFile('foto') ){
            $path = $req->file('foto')->store('public');
            $split = explode('/', $path);
            $jugador->foto = $split[1];
        }
        else{
            $jugador->foto = 'default.png';
        }
        $jugador->sexo = $req->sexo;
        $jugador->fechaNacimiento = $req->fecha_nacimiento;
        $jugador->save();
        return redirect( '/jugador' );
    }

    public function show($curp) {
        $jugador = JugadorModel::where('curp', '=', $curp)->first();
        $titulo = 'Detalle de jugador.';
        return view( '/jugador/show', compact( 'jugador', 'titulo' ) );
    }

    public function edit($curp) {
        $jugador = JugadorModel::where('curp', '=', $curp)->first();
        $titulo = 'Edición de jugador.';
        return view( '/jugador/edit', compact( 'jugador', 'titulo' ) );
    }

    public function update(Request $req, $curp) {
        if( $req->hasFile('foto') ){
            $path = $req->file('foto')->store('public');
            $split = explode('/', $path);
            JugadorModel::where( 'curp', $curp )->update( [
                'nombreCompleto'=>ucwords($req->nombre),
                'sexo'=>$req->sexo,
                'fechaNacimiento'=>$req->fecha_nacimiento,
                'foto'=>$split[1]
            ] );
            return redirect( '/jugador' );
        }
        JugadorModel::where( 'curp', $curp )->update( [
            'nombreCompleto'=>ucwords($req->nombre),
            'sexo'=>$req->sexo,
            'fechaNacimiento'=>$req->fecha_nacimiento
        ] );
        return redirect( '/jugador' );
    }

    public function destroy($curp) {
        $jugador = JugadorModel::where('curp', '=', $curp)->delete();
        return redirect( '/jugador' );
    }
}
