<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\EstadoModel;

class EstadoControl extends Controller {
    public function index () {
        $lista = EstadoModel::orderBy( 'id', 'DESC' )->paginate( 5 )->setPageName( 'pagina' );
        $tamano = count( EstadoModel::all() );
        $titulo = 'Lista de estados registrados.';
        return view( '/estado/index', compact( 'lista', 'tamano', 'titulo' ) );
    }

    public function create () {
        $titulo = 'Registro de estados.';
        return view( '/estado/create', compact( 'titulo' ) );
    }

    public function store ( Request $req ) {
        $estado = new EstadoModel;
        $estado->nombre = ucwords($req->nombre);
        $estado->save();
        return redirect( '/estado' );
    }

    public function show ( $id ) {
        $titulo = 'Detalle de estado.';
        $estado = EstadoModel::find( $id );
        return view( '/estado/show', compact( 'estado', 'titulo' ) );
    }

    public function edit ( $id ) {
        $titulo = 'Edición de estado.';
        $estado = EstadoModel::find( $id );
        return view( '/estado/edit', compact( 'estado', 'titulo' ) );
    }

    public function update ( Request $req, $id ) {
        EstadoModel::where( 'id', $id )->update( [
            'nombre'=>ucwords($req->nombre)
        ] );
        return redirect( '/estado' );
    }

    public function destroy ( $id ) {
        $estado = EstadoModel::find( $id );        
        $estado->delete( );
        return redirect( '/estado' );
    }
}
