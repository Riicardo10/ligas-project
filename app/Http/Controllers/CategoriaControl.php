<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\CategoriaModel;

class CategoriaControl extends Controller {
    public function index() {
        $lista = CategoriaModel::orderBy( 'id', 'ASC' )->paginate( 5 );
        $tamano = count( CategoriaModel::all() );
        $titulo = 'Lista de categorías registradas.';
        return view( '/categoria/index', compact( 'lista', 'tamano', 'titulo' ) );
    }

    public function create() {
        $titulo = 'Registro de categorías.';
        return view( '/categoria/create', compact( 'titulo' ) );
    }

    public function store(Request $req) {
        $categoria = new CategoriaModel;
        $categoria->nombre = ucfirst( $req->nombre );
        $categoria->descripcion = ucfirst( $req->descripcion );
        $categoria->save();
        return redirect( '/categoria' );
    }

    public function show($id) {
        $titulo = 'Detalle de categoría.';
        $categoria = CategoriaModel::find( $id );
        return view( '/categoria/show', compact( 'categoria', 'titulo' ) );
    }

    public function edit($id) {
        $titulo = 'Edición de categoría.';
        $categoria = CategoriaModel::find( $id );
        return view( '/categoria/edit', compact( 'categoria', 'titulo' ) );
    }

    public function update(Request $req, $id) {
        CategoriaModel::where( 'id', $id )->update( [
            'nombre'=>ucfirst( $req->nombre ),
            'descripcion'=>ucfirst( $req->descripcion )
        ] );
        return redirect( '/categoria' );
    }

    public function destroy($id) {
        $categoria = CategoriaModel::find( $id );        
        $categoria->delete( );
        return redirect( '/categoria' );
    }
}
