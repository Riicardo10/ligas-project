<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\EstadoModel;
use App\LigaModel;

class LigaControl extends Controller {
    public function index() {
        $lista = DB::table('liga')
            ->join('ciudad', 'liga.idCiudad', '=', 'ciudad.id')
            ->join('estado', 'ciudad.idEstado', '=', 'estado.id')
            ->select('liga.*', 'ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'estado.id AS idEstado', 'estado.nombre AS nombreEstado')
            ->orderBy('liga.id', 'ASC')
            ->paginate( 5 );
        $tamano = count( LigaModel::all() );
        $titulo = 'Lista de ligas registradas.';
        return view( '/liga/index', compact( 'lista', 'tamano', 'titulo' ) );
    }

    public function create() {
        $anio = Date('Y');
        $titulo = 'Registro de ligas';
        $lista_estados = EstadoModel::all();
        return view( '/liga/create', compact( 'anio', 'lista_estados', 'titulo' ));
    }

    public function store(Request $req) {
        if( $req->ciudad == 0 ){
            return redirect( '/liga' );
        }
        $liga = new LigaModel;
        $liga->nombre = ucfirst( $req->nombre );
        $liga->anio = $req->anio;
        $liga->email = $req->email;
        $liga->usuario = $req->usuario;
        $liga->contrasenia = $req->contrasenia;
        $liga->idCiudad = $req->ciudad;
        $liga->save();
        return redirect( '/liga' );
    }

    public function show($id) {
        $liga = DB::table('liga')
            ->join('ciudad', 'liga.idCiudad', '=', 'ciudad.id')
            ->join('estado', 'ciudad.idEstado', '=', 'estado.id')
            ->select('liga.id AS idLiga', 'liga.nombre AS nombreLiga', 'liga.anio AS anioLiga', 'liga.email AS emailLiga', 'liga.usuario AS usuarioLiga', 'liga.contrasenia AS contraseniaLiga', 'ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'estado.id AS idEstado', 'estado.nombre AS nombreEstado')
            ->where( 'liga.id', '=', $id )
            ->get()->first();
        $titulo = 'Detalle de liga.';
        return view( '/liga/show', compact( 'liga', 'titulo' ) );
    }

    public function edit($id) {
        $lista_estados = EstadoModel::all();
        $liga = DB::table('liga')
            ->join('ciudad', 'liga.idCiudad', '=', 'ciudad.id')
            ->join('estado', 'ciudad.idEstado', '=', 'estado.id')
            ->select('liga.id AS idLiga', 'liga.nombre AS nombreLiga', 'liga.anio AS anioLiga', 'liga.email AS emailLiga', 'liga.usuario AS usuarioLiga', 'liga.contrasenia AS contraseniaLiga', 'ciudad.id AS idCiudad', 'ciudad.nombre AS nombreCiudad', 'estado.id AS idEstado', 'estado.nombre AS nombreEstado')
            ->where( 'liga.id', '=', $id )
            ->get()->first();
        $titulo = 'Edición de liga.';
        return view( '/liga/edit', compact( 'liga', 'lista_estados', 'titulo' ) );
    }

    public function update(Request $req, $id) {
        LigaModel::where( 'id', $id )->update( [
            'nombre'=>ucfirst($req->nombre),
            'email'=>$req->email,
            'idCiudad'=>$req->ciudad
        ] );
        return redirect( '/liga' );
    }

    public function destroy($id) {
        $liga = LigaModel::find( $id );        
        $liga->delete( );
        return redirect( '/liga' );
    }
}
