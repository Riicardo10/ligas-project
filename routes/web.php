<?php

Route::get('/', function () {
    return view('welcome');
});

Route::resource('estado', 'EstadoControl');
Route::resource('ciudad', 'CiudadControl');
Route::resource('jugador', 'JugadorControl');
Route::resource('categoria', 'CategoriaControl');
Route::resource('liga', 'LigaControl');
Route::resource('equipo', 'EquipoControl');


// ====================================================================================
// PARA RECURSOS DE LIGA->CREATE
// ====================================================================================
Route::post('/ciudad/ciudades/json/{idEstado}', function(Request $request, $idEstado){
    $sql = "SELECT ciudad.id AS idCiudad, ciudad.nombre AS nombreCiudad FROM ciudad WHERE idEstado = ?";
    $ciudad = DB::select($sql, array( $idEstado ) );
    return response()->json($ciudad);
} );
// ====================================================================================
// PARA OBTENER EL NOMBRE DE LA LIGA EN BASE AL ESTADO/CIUDAD DE EQUIPO->CREATE
// ====================================================================================
Route::post('/liga/ciudad/json/{idCiudad}', function(Request $request, $usuario){
    $sql = "SELECT liga.id AS idLiga, liga.nombre AS nombreLiga, liga.anio AS anioLiga FROM liga WHERE idCiudad = ?";
    $liga = DB::select($sql, array( $usuario ) );
    return response()->json($liga);
} );
// ====================================================================================
// PARA COMPROBAR NOMBRE DE USUARIO DE LIGA->CREATE
// ====================================================================================
Route::post('/liga/usuario/json/{usuario}', function(Request $request, $usuario){
    $sql = "SELECT usuario FROM liga WHERE usuario = ?";
    $liga = DB::select($sql, array( $usuario ) );
    if( $liga )
        return response()->json( ['existe'=>true] );
    return response()->json( ['existe'=>false] );
} );
// ====================================================================================
// PARA COMPROBAR NOMBRE DE USUARIO DE EQUIPO->CREATE
// ====================================================================================
Route::post('/equipo/usuario/json/{usuario}', function(Request $request, $usuario){
    $sql = "SELECT * FROM equipo WHERE usuario = ?";
    $liga = DB::select($sql, array( $usuario ) );
    if( $liga )
        return response()->json( ['existe'=>true] );
    return response()->json( ['existe'=>false] );
} );



























// Route::post( '/ciudad/json/{idEstado}', function(Request $req, $idEstado ) {
//     $sql = "SELECT ciudad.id AS idCiudad, ciudad.nombre AS nombreCiudad FROM ciudad WHERE idEstado = ?";
//     $ciudad = DB::select($sql, array( $idEstado ) );
//     return response()->json($ciudad);
// } );
// Route::get( '/ciudad/json/{idEstado}', function( Request $req, $idEstado ) {
//     $sql = "SELECT ciudad.id AS idCiudad, ciudad.nombre AS nombreCiudad FROM ciudad WHERE idEstado = ?";
//     $ciudad = DB::select($sql, array( $idEstado ) );
//     return response()->json($ciudad);
// } );